const express = require("express");

// mongoose is a package that allows us to create schemas to model our data strcutures and to manipulate our database using different access methods.
const mongoose = require("mongoose");

const port = 3001;

const app = express();

	// [SECTION] MongoDB Connection
		/*
			Syntax:
				mongoose.connect("mongoDBconnectionString", {
					options to avoid errors in our connection
				})
		*/

	mongoose.connect("mongodb+srv://admin:admin@batch245-manabat.mca9yeu.mongodb.net/s35-discussion?retryWrites=true&w=majority",
			{
				// allows us to avoid any current and future errors while connecting to mongoDB
				useNewUrlParser: true,
				useUnifiedTopology: true
			})

	let db = mongoose.connection;

	// error handling in connecting 
	db.on("error", console.error.bind(console, "Connection Error"));

	// this will be triggerred if the connection is successful
	db.once("open", ()=> console.log(`We're connected to the cloud databaes!`));

		// mongoose schemas
			// schemas determine the structure of the documents to be written in the database
			// schemas act as a blueprint to our data
			/*
				Syntax:
					const schemaName = new mongoose.Schema({keyvaluepairs})
			*/
		// taskSchema
		// required - 
		// default - is used if a field value if not supplied
		const taskSchema = new mongoose.Schema({
				name: {
					type: String,
					required: [true, "Task name is required"]
				},
				status : {
					type: String,
					default: "pending"
				}
		})

// Activity Schema
		const userSchema = new mongoose.Schema({
				username: {
					type: String,
					required: [true, "Username is required"]
				},
				password: {
					type: String,
					default: "pending"
				}
		})

		// [SECTION] Models
			// uses schema to create/instatiate documents/objects that follows our schema structure
			// the variable/object that will be created can be used to run commands with our database
			/*
				Syntax:
				 const variableName = mongoose.model("collectionName", schemaName);
			*/

		const Task = mongoose.model("Task", taskSchema)

// Activity Model
		const User = mongoose.model("User", userSchema)

// middlewares
app.use(express.json()); // allows the app to read json data
app.use(express.urlencoded({extended: true})) // it will allow our app to read data from forms.

// [SECTION] routing
	// create/add new task
		// 1. check if the task is existing.
			// if ask already exist in the database, we will return a message "the task is already existing!"
			// if the task doesnt exist in the database, we will add it in the database
	app.post("/tasks", (request, response) => {
		let input = request.body

			console.log(input.status);
			if(input.status === undefined){
				Task.findOne({name: input.name}, (error, result) => {
								console.log(result);

								if(result !== null){
									return response.send("The task is already existing!")
								}
								else {
									let newTask = new Task({
										name: input.name
									})

									// save() method will save the object in the collection that the object instatiated
									newTask.save((saveError, savedTask) =>{
										if(saveError){
											return console.log(saveError);
										}
										else {
											return response.send("New task created!")
										}
									})
								}
							})
			}
			else {
				Task.findOne({name: input.name}, (error, result) => {
								console.log(result);

								if(result !== null){
									return response.send("The task is already existing!")
								}
								else {
									let newTask = new Task({
										name: input.name,
										status: input.status
									})

									// save() method will save the object in the collection that the object instatiated
									newTask.save((saveError, savedTask) =>{
										if(saveError){
											return console.log(saveError);
										}
										else {
											return response.send("New task created!")
										}
									})
								}
							})
			}
/*			Task.findOne({name: input.name}, (error, result) => {
				console.log(result);

				if(result !== null){
					return response.send("The task is already existing!")
				}
				else {
					let newTask = new Task({
						name: input.name
					})

					// save() method will save the object in the collection that the object instatiated
					newTask.save((saveError, savedTask) =>{
						if(saveError){
							return console.log(saveError);
						}
						else {
							return response.send("New task created!")
						}
					})
				}
			})*/
	})

// ACTIVITY Route
	app.post("/signup", (request, response) => {
		let input = request.body

			User.findOne({username: input.username}, (error, result) => {
				console.log(result);

				if(result !== null){
					return response.send(`Username: ${input.username} is already existing!`)
				}
				else {
					let newUser = new User({
						username: input.username,
						password: input.password
					})

					newUser.save((saveError, savedTask) =>{
						if(saveError){
							return console.log(saveError);
						}
						else {
							return response.send(`Username: ${input.username} is now registered!`)
						}
					})
				}
			})
	})

// [SECTION] retrieving all the tasks
	app.get("/tasks", (request, response) => {
		Task.find({}, (error, result) => {
			if(error) {
				console.log(error);
			}
			else {
				return response.send(result);
			}
		})
	})

app.listen(port, ()=> console.log(`Server is running at port ${port}!`));